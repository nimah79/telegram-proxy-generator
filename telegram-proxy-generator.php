<?php

/**
 * SOCKS5 Proxy Generator for Telegram
 * Uses hotgram.ir proxies
 * By NimaH79
 * NimaH79.ir.
 */
function generateTelegramProxy()
{
    $ch = curl_init('http://lh'.mt_rand(1, 83).'.hotgram.ir/v1/proxy');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result, true);
    $result = $result['data'][0];
    $result = openssl_decrypt($result, 'AES-128-CBC', 'KCH@LQj#>6VCqqLg', 0, 'YC\'2bmK=b%#NQ?9j');
    $result = json_decode($result, true);
    $proxy = 'https://t.me/socks?server='.$result['ip'].'&port='.$result['prt'].'&user='.$result['usr'].'&pass='.$result['pwd'];

    return $proxy;
}

echo generateTelegramProxy();
